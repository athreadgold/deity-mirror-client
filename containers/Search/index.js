import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { load } from '../../redux/modules/search';
import ProductList from './../../components/ProductList';
import styles from './styles.scss';

class Search extends Component {
  constructor() {
    super();
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentDidMount() {
    this.props.load('');
  }

  handleSearch(e) {
    const searchValue = e.target.value;

    this.props.load(searchValue);
  }

  render () {
    const { data } = this.props.search;
    const products = data && data.data && data.data.product;

    return (
      <div className={styles.search}>
        <input placeholder="Search for products..." type="text" onChange={this.handleSearch}/>
        {products && <ProductList products={products.hits} />}
      </div>
    )
  }
}

const mapStateToProps = ({ search }) => ({ search });

const mapDispatchToProps = (dispatch) => {
  return {
    load: bindActionCreators(load, dispatch),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Search)
